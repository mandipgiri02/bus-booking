import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from "react-native";

const CustomButton = (props) => {
    const {title, onPress} = props;
    return (
        <TouchableOpacity style={styles.mainStyle}
                          onPress={() => onPress()}>
            <Text style={styles.textStyle}>{title}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    mainStyle: {
        padding: 20,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        shadowOffset: {width: 0, height: 2},//IOS
        shadowOpacity: 0.4,//IOS
        elevation: 5,//ANDROID
    },
    textStyle: {
        fontSize: 16,
        color: 'white'
    }
});

export default CustomButton