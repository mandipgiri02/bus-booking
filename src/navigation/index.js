import React, {Component} from "react";
import {createAppContainer, createStackNavigator} from "react-navigation";
import MainForm from "../features/mainForm/components";
import LocationSelection from "../features/locationSelection/components";

const RootStack = createStackNavigator(
    {
        MainForm: MainForm,
        LocationSelection:LocationSelection
    },
    {
        initialRouteName: 'MainForm',
        headerMode: 'none'
    }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
    render() {
        return <AppContainer/>
    }
}