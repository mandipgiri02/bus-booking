import {PICK_UP_LOCATION} from "../../../utils/Constant";

const initialState = {
    data: null
};

export default function pickUpLocationReducer(state = initialState, action) {
    if (action.type === PICK_UP_LOCATION) {
        return {
            ...state,
            data: action.location
        }
    }else {
        return state
    }

}