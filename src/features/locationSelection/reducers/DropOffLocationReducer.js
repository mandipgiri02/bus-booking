import {DROP_OFF_LOCATION} from "../../../utils/Constant";

const initialState = {
    data: null
};

export default function dropOffLocationReducer(state = initialState, action) {
    if (action.type === DROP_OFF_LOCATION) {
        return{
            ...state,
            data:action.location
        }
    }else {
        return state
    }

}