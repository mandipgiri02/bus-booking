import {PICK_UP_LOCATION} from "../../../utils/Constant";

export function storePickUpLocation(location) {
    return (dispatch) => {
        dispatch(pickUpLocation(location))
    }
}

export function pickUpLocation(location) {
    return {
        type: PICK_UP_LOCATION,
        location
    }
}