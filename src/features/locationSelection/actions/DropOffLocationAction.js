import {DROP_OFF_LOCATION} from "../../../utils/Constant";

export function storeDropOffLocation(location) {
    return (dispatch) => {
        dispatch(dropOffLocation(location))
    }
}

export function dropOffLocation(location) {
    return {
        type: DROP_OFF_LOCATION,
        location
    }
}