import React, {Component} from 'react';
import {View} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import CustomButton from "../../../components/customButton";
import {connect} from 'react-redux';
import {storePickUpLocation} from "../actions/PickUpLocationAction"
import {storeDropOffLocation} from "../actions/DropOffLocationAction"
import Toast, {DURATION} from "react-native-easy-toast";

class LocationSelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: this.props.navigation.getParam('status', 'Pick Up'),
            latitude: 27.6673901,
            longitude: 85.3128871
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <MapView
                    style={{flex: 1}}
                    //using google as provider
                    provider={PROVIDER_GOOGLE}
                    initialRegion={{
                        latitude: 27.6673901,
                        longitude: 85.3128871,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    onPress={(e) => {
                        this.setState({
                            latitude: e.nativeEvent.coordinate.latitude,
                            longitude: e.nativeEvent.coordinate.longitude,
                        }, () => {
                            this.saveUserCoordinates()
                        })
                    }}
                >
                    <Marker
                        coordinate={{
                            latitude: this.state.latitude,
                            longitude: this.state.longitude
                        }}
                        title={'title'}
                        description={'description'}
                    />
                </MapView>
                <CustomButton
                    title={`Please tap in the location on the map to select your ${this.state.status} location`}
                    onPress={() => null}/>
                <Toast
                    ref="toast"
                    style={{backgroundColor: 'green', marginBottom: 200}}
                    position='bottom'
                    textStyle={{color: 'white'}}
                />
            </View>
        );
    }

    saveUserCoordinates = () => {
        const val = {
            lat: this.state.latitude,
            lng: this.state.longitude
        };
        if (this.state.status === 'Pick Up') {
            this.refs.toast.show("Pick Up location stored successfully", DURATION.LENGTH_SHORT);
            this.props.storePickUpLocation(val);
        } else {
            this.refs.toast.show("Drop Off location stored successfully", DURATION.LENGTH_SHORT);
            this.props.storeDropOffLocation(val);

        }
        setTimeout(() => {
            this.props.navigation.goBack();
        }, 800);
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {
        storePickUpLocation: (location) => dispatch(storePickUpLocation(location)),
        storeDropOffLocation: (location) => dispatch(storeDropOffLocation(location))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationSelection);