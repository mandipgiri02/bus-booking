import React, {Component} from "react";
import {StyleSheet, Text, View} from "react-native";
import CustomButton from "../../../components/customButton";
import DateTimePicker from 'react-native-modal-datetime-picker';
import Toast, {DURATION} from "react-native-easy-toast";
import {connect} from 'react-redux'
import {storePickUpTime} from "../actions/PickUpTimeAction";
import {storeDropOffTime} from "../actions/DropOffTimeAction";
import {fullDateHelper} from "../../../utils/Helper";


class MainForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pickUpFlag: false,
            dropOffFlag: false,
            pickUpTime: null,
            dropOffTime: null,
            pickUpLocation: null,
            dropOffLocation: null,
            datePickerFlag: false,
            completionFlag: false
        }
    }

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        if (nextProps.pickUpLocationReducer !== this.props.pickUpLocationReducer) {
            //pick up location action changes applied here
            this.setState({
                pickUpLocation: nextProps.pickUpLocationReducer.data
            })
        }

        if (nextProps.dropOffLocationReducer !== this.props.dropOffLocationReducer) {
            //drop off location action changes applied here
            this.setState({
                dropOffLocation: nextProps.dropOffLocationReducer.data
            })
        }
        return true
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Text style={styles.titleTextStyle}>Welcome to Bus Booking App</Text>
                <Text style={styles.titleTextStyle}>Please Follow the steps</Text>
                <SubContainer title={"Select Pick Up Time"}
                              onPress={() => this.showDatePicker('pickUp')}/>
                {this.state.pickUpFlag ?
                    <SubContainer title={"Select Pick Location"}
                                  onPress={() => this.props.navigation.navigate('LocationSelection',
                                      {status: 'Pick Up'})}/> : null}
                {this.state.pickUpLocation ? <SubContainer title={"Select Drop Off Time"}
                                                           onPress={() => this.showDatePicker('dropOff')}/> : null}
                {this.state.dropOffFlag ? <SubContainer title={"Select Drop Off Location"}
                                                        onPress={() => this.props.navigation.navigate('LocationSelection',
                                                            {status: 'Drop Off'})}/> : null}
                {this.state.dropOffLocation ? <SubContainer title={"Reserve"}
                                                            onPress={() => this.setState({completionFlag: true})}/> : null}
                {this.state.completionFlag ?
                    <View style={{justifyContent: 'center', alignItems: 'center',paddingLeft:20,paddingRight:20}}>
                        <Text style={{color: 'green'}}>Reservation Successful</Text>
                        <Text style={{color: 'black'}}> Your pick up is scheduled at {fullDateHelper(this.state.pickUpTime)} in {this.state.pickUpLocation.lat},{this.state.pickUpLocation.lng}</Text>

                    </View>
                    : null}
                <DateTimePicker onConfirm={(date) => this.setDate(date)}
                                onCancel={() => this.dismissDatePicker()}
                                isVisible={this.state.datePickerFlag}
                                mode={'datetime'}/>
                <Toast
                    ref="toast"
                    style={{backgroundColor: 'black', marginBottom: 200}}
                    position='bottom'
                    textStyle={{color: 'white'}}
                />
            </View>
        );
    }

    setDate = (date) => {
        //function that stores pick up or drop off time
        if (this.state.pickUpTime === null) {
            //pick up time
            const msToHour = (1000 * 60 * 60);
            let currentTime = new Date();
            let selectedTime = new Date(date);
            let timeDifference = (selectedTime - currentTime) / (msToHour);
            //checking if selected time is 3 hours or more from current time
            if (timeDifference > 3) {
                //checking if selected time is more than 8 hours from current time
                if (timeDifference <= 8) {
                    this.setState({
                        pickUpTime: date,
                        datePickerFlag: false,
                    })
                } else {
                    this.setState({
                        datePickerFlag: false,
                        pickUpFlag: false
                    }, () => {
                        this.refs.toast.show("Pick Up time should be within 8 hours of current time", DURATION.LENGTH_SHORT);
                    });
                }
            } else {
                this.setState({
                    datePickerFlag: false,
                    pickUpFlag: false
                }, () => {
                    this.refs.toast.show("Pick Up time should be at least 3 hours ahead of current time", DURATION.LENGTH_SHORT);
                });
            }
        } else {
            let pickUpTime = new Date(this.state.pickUpTime);
            let selectedTime = new Date(date);
            if (selectedTime > pickUpTime) {
                this.setState({
                    dropOffTime: date,
                    datePickerFlag: false,
                })
            } else {
                this.setState({
                    datePickerFlag: false,
                    dropOffFlag: false
                }, () => {
                    this.refs.toast.show("Drop Off time should be later than pick up time", DURATION.LENGTH_SHORT);
                });
            }
        }

    };

    showDatePicker = (type) => {
        //function to show date picker for pickup or drop off
        if (type === 'pickUp') {
            this.setState({
                datePickerFlag: true,
                pickUpFlag: true
            })
        } else {
            this.setState({
                datePickerFlag: true,
                dropOffFlag: true
            })
        }
    };

    dismissDatePicker = () => {
        //function to hide date picker for pickup or drop off
        if (this.state.pickUpTime === null) {
            this.setState({
                datePickerFlag: false,
                pickUpFlag: false
            })
        } else {
            this.setState({
                datePickerFlag: false,
                dropOffFlag: false
            })
        }

    }

}

function mapStateToProps(state) {
    return {
        pickUpLocationReducer: state.pickUpLocationReducer,
        dropOffLocationReducer: state.dropOffLocationReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        storePickUpTime: (time) => dispatch(storePickUpTime(time)),
        storeDropOffTime: (time) => dispatch(storeDropOffTime(time)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainForm);

export const SubContainer = (props) => {
    const {title, onPress} = props;
    return (
        <View>
            <CustomButton title={title}
                          onPress={onPress}/>
            <View style={styles.rowContainer}/>
        </View>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 5
    },
    labelTextStyle: {
        fontSize: 18,
        color: 'grey'
    },
    titleTextStyle: {
        fontSize: 20,
        color: 'red',
        marginBottom: 20
    }
});
