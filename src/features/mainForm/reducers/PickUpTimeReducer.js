import {PICK_UP_TIME} from "../../../utils/Constant";

const initialState = {
    data: null
};

export default function pickUpTimeReducer(state = initialState, action) {
    if (action.type === PICK_UP_TIME) {
        return{
            ...state,
            data:action.time
        }
    }else {
        return state
    }

}