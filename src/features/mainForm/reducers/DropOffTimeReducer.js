import {DROP_OFF_TIME} from "../../../utils/Constant";

const initialState = {
    data: null
};

export default function pickUpTimeReducer(state = initialState, action) {
    if (action.type === DROP_OFF_TIME) {
        return{
            ...state,
            data:action.location
        }
    }else {
        return state
    }

}