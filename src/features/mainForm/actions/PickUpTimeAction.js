import {PICK_UP_TIME} from "../../../utils/Constant";

export function storePickUpTime(time) {
    return (dispatch) => {
        dispatch(pickUpTime(time))
    }
}

export function pickUpTime(time) {
    return {
        type: PICK_UP_TIME,
        time
    }
}