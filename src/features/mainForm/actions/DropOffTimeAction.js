import {DROP_OFF_TIME} from "../../../utils/Constant";

export function storeDropOffTime(time) {
    return (dispatch) => {
        dispatch(dropOffTime(time))
    }
}

export function dropOffTime(time) {
    return {
        type: DROP_OFF_TIME,
        time
    }
}