import {combineReducers} from 'redux';
import pickUpLocationReducer from '../features/locationSelection/reducers/PickUpLocationReducer'
import dropOffLocationReducer from '../features/locationSelection/reducers/DropOffLocationReducer'
import pickUpTimeReducer from '../features/mainForm/reducers/PickUpTimeReducer';
import dropOffTimeReducer from '../features/mainForm/reducers/DropOffTimeReducer';

const rootReducer = combineReducers({
    pickUpLocationReducer,
    dropOffLocationReducer,
    pickUpTimeReducer,
    dropOffTimeReducer
});

export default rootReducer;