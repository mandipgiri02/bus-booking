export const fullDateHelper = (string) => {
    //function that returns date in Y/M/D H:M format as string
    let date = new Date(string);

    let year = date.getFullYear();
    let month = date.getMonth();
    let day = date.getDate();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    month = month + 1;
    month = month < 10 ? `0${month}` : month;
    day = day < 10 ? `0${day}` : day;
    hours = hours < 10 ? `0${hours}` : hours;
    minutes = minutes < 10 ? `0${minutes}` : minutes;

    let strDate = `${year}/${month}/${day}  ${hours}:${minutes}`;

    return (strDate);
};